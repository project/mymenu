
Description: Dropdown personal menu in a block

http://drupal.org/project/mymenu


The idea is to show a menu in a block (to authenticated users) that allows navigation to parts of the website not covered by for example primary links.

The menu is a dropdown list, contains just one level of items from a specified starting point in menu tree.
It was degined to working on traditional Desktop browers like IE/FF/Safari/Chrome, but also on Devices like an iphone.

Example: in the navigation menu, create an Item "My Menu" and then drag in the 
 navigation menu items you like as children.
Configuration: Enable the block and then set some variables in settings.php:

$conf['mymenu_useristitle'] = '1';         // Username is the menu title
$conf['mymenu_title'] = 'My Menu';         // Or, set Title of the closed dropdown
$conf['mymenu_blocktitle'] = '<none>';     // Title for the block
$conf['mymenu_menutop'] = 'navigation';    // Which menu
$conf['mymenu_menutree'] = 'My Menu';      // Top level item in the Menu tree


